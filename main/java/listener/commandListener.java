package listener;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

import core.commandHandler;
import util.Statics;

import java.util.logging.Logger;

import static util.Statics.config;

public class commandListener extends ListenerAdapter{

    public void onMessageReceived(MessageReceivedEvent event) {

        if (event.getMessage().getContent().startsWith(config.getProperty("prefix")) && !event.getMessage().getAuthor().isBot()) {

            //Initialise Logger
            Logger logger = Logger.getLogger(commandListener.class.getName());

            //Start writing Log in File
            logger.addHandler(Statics.fileHandler);

            logger.info("Triggered commandListener");
            event.getMessage().delete().queue();
            commandHandler.handle(event.getMessage().getContent().substring(config.getProperty("prefix").length()), event);

        }
    }

}
