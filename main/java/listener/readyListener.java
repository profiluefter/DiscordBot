package listener;

import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import util.Statics;
import static util.Statics.config;

import java.util.logging.*;

public class readyListener extends ListenerAdapter{

    public void onReady(ReadyEvent event) {

        //Initialise Logger
        Logger logger = Logger.getLogger(readyListener.class.getName());

        //Start writing Log in File
        logger.addHandler(Statics.fileHandler);

        logger.info("Finished starting Bot!");

        StringBuilder out = new StringBuilder("\n This bot is running on following servers: \n");

        for ( Guild g : event.getJDA().getGuilds() ) {

            out.append(g.getName()).append("\n");

        }

        System.out.println(out);

        System.out.println("Current Prefix of the Bot is \"" + config.getProperty("prefix") + "\" !");

    }

}
