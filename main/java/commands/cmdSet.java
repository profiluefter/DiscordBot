package commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import static util.Statics.config;

import java.awt.*;
import java.util.ArrayList;

public class cmdSet implements Command{

    @Override
    public String help() {
        return "Set some Properties of the Bot!";
    }

    @Override
    public void action(ArrayList<String> args, MessageReceivedEvent event) {

        if(args.size() != 2) {
            event.getTextChannel().sendMessage(new EmbedBuilder().setColor(Color.red).setTitle("Error!").setDescription("This Command only works with 2 Parameter!").build()).queue();
        } else {
            if(config.entryExist(args.get(0))) {
                event.getTextChannel().sendMessage(new EmbedBuilder().setColor(Color.green).setTitle("Success!").setDescription("Successfully changed Value \"" + args.get(0) + "\" to \"" + args.get(1) + "\"!").build()).queue();
                config.setProperty(args.get(0), args.get(1));
                config.writeProperties();
            } else {
                event.getTextChannel().sendMessage(new EmbedBuilder().setColor(Color.red).setTitle("Error!").setDescription("Property " + args.get(0) + " not exists!").build()).queue();
            }
        }

    }

    @Override
    public boolean requirements(ArrayList<String> args, MessageReceivedEvent event) {
        return true;
    }
}
