package commands;

import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.EmbedBuilder;

import java.awt.*;
import java.util.ArrayList;

public class cmdPing implements Command{

    @Override
    public String help() {
        return "Outputs the Ping of the Bot";
    }

    @Override
    public void action(ArrayList<String> args, MessageReceivedEvent event) {
        event.getTextChannel().sendMessage(
                new EmbedBuilder().setColor(Color.blue).setTitle("Pong!").setDescription("Ping: " + event.getJDA().getPing() + "ms").build()
        ).queue();
    }

    @Override
    public boolean requirements(ArrayList<String> args, MessageReceivedEvent event) {
        return true;
    }
}