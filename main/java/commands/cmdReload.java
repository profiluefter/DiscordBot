package commands;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;

import java.util.ArrayList;

import static util.Statics.config;

public class cmdReload implements Command{

    @Override
    public String help() {
        return "Reloads the Settings of the Bot";
    }

    @Override
    public void action(ArrayList<String> args, MessageReceivedEvent event) {
        config.readProperties();
        event.getTextChannel().sendMessage(new EmbedBuilder().setTitle("Success!").setDescription("Successfully reloaded Bot-Config!").build()).queue();
    }

    @Override
    public boolean requirements(ArrayList<String> args, MessageReceivedEvent event) {
        return true;
    }
}
