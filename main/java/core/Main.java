package core;

//TODO: Permissions

import commands.cmdHelp;
import commands.cmdPing;
import commands.cmdReload;
import commands.cmdSet;
import listener.commandListener;
import listener.readyListener;
import static util.Statics.config;
import util.Statics;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.OnlineStatus;
import net.dv8tion.jda.core.entities.Game;
import net.dv8tion.jda.core.exceptions.RateLimitedException;

import java.io.File;
import java.util.logging.*;
import javax.security.auth.login.LoginException;

public class Main {
	

	public static void main(String[] args) {

		//Initialise Config
		config.readProperties();
		config.addProperty("token");
		config.addProperty("prefix");
		config.addProperty("logger");
		config.writeProperties();

		//Initialise Main Logger
		Logger logger = Logger.getLogger(Main.class.getName());

		//Check for Log-Directory
		File f = new File(System.getProperty("user.dir") + "/logs/");
		if(!f.exists()) {
			try {
				if(f.mkdir())
					logger.info("Created log folder!");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		//Init Static
		Statics.init();
		logger.addHandler(Statics.fileHandler);

		//Check Config
		if(!(config.getProperty("token").length() == 59)) {
			System.err.println("[ERROR] Bot-Token not valid!");
			config.writeProperties();
			System.exit(-1);
		} else {
			logger.info("Found valid Bot-Token!");
		}

		//Initialise Bot
		JDABuilder builder = new JDABuilder(AccountType.BOT);
		builder.setToken(config.getProperty("token"));
		builder.setAutoReconnect(true);
		builder.setStatus(OnlineStatus.ONLINE);
		builder.setGame(Game.of("noch nichts!"));

		//Add commands
		logger.info("Adding Commands!");
		commandHandler.add("ping", new cmdPing());
		commandHandler.add("help", new cmdHelp());
		commandHandler.add("reload", new cmdReload());
		commandHandler.add("set", new cmdSet());

		//Add listeners
		logger.info("Adding Listeners!");
		builder.addEventListener(new readyListener());
		builder.addEventListener(new commandListener());

		//Start Bot
		logger.info("Attempting to start Bot!");
		try {
			@SuppressWarnings("unused") JDA bot = builder.buildBlocking();
		} catch (LoginException | IllegalArgumentException | InterruptedException | RateLimitedException e) {
			e.printStackTrace();
		}

	}
	
}
