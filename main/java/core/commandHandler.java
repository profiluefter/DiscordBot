package core;

import commands.Command;

import net.dv8tion.jda.core.EmbedBuilder;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import util.Statics;

import java.awt.*;
import java.util.*;
import java.util.logging.Logger;

public class commandHandler {

    private static Map<String, Command> commandContainer = new HashMap<>();
    private static Map<Command, String> commandContainer2 = new HashMap<>();

    public static void handle(String command, MessageReceivedEvent event) {

        //Initialise Logger
        Logger logger = Logger.getLogger(commandHandler.class.getName());

        //Start writing Log in File
        logger.addHandler(Statics.fileHandler);

        ArrayList<String> split = new ArrayList<>();

        Collections.addAll(split, command.split(" "));

        String com = split.get(0);
        ArrayList<String> arg = new ArrayList<>();
        split.remove(0);
        arg.addAll(split);

        StringBuilder args = new StringBuilder();
        for(String s : arg)
            args.append("\"").append(s).append("\"").append(" ");

        if(commandContainer.containsKey(com)) {
            if (commandContainer.get(com).requirements(arg, event)) {
                commandContainer.get(com).action(arg, event);
                logger.info(event.getAuthor().getName() + " used Command \"" + com + "\" with arguments " + args.toString());
            } else {
                event.getTextChannel().sendMessage(new EmbedBuilder().setTitle("Sorry!").setDescription("You don't have enought permission to do that!").build()).queue();
                logger.warning(event.getAuthor().getName() + " tried to use Command \"" + com + "\" with arguments " + args.toString() + " without Permissions");
            }
        } else {
            event.getTextChannel().sendMessage(new EmbedBuilder().setColor(Color.red).setTitle("Error").setDescription("Error! Command " + com + " not found!").build()).queue();
            logger.info(event.getAuthor().getName() + " tried to use Command \"" + com + "\" with arguments " + args.toString() + "! But that Command doesn't exist!");
        }

    }

    static void add(String string, Command c){

        //Initialise Logger
        Logger logger = Logger.getLogger(commandHandler.class.getName());

        //Start writing Log in File
        logger.addHandler(Statics.fileHandler);

        logger.info("Added Command " + string + "!");
        commandContainer.put(string, c);
        commandContainer2.put(c, string);

    }

    public static String getName(Command command) {
        return commandContainer2.get(command);
    }

    public static ArrayList<Command> getCommands() {

        ArrayList<Command> commandArrayList = new ArrayList<>();

        for (String key : commandContainer.keySet()) {
            commandArrayList.add(commandContainer.get(key));
        }

        return commandArrayList;

    }

}