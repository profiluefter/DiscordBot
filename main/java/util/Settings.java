package util;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

public class Settings {

    private Properties config = new Properties();
    private ArrayList<String> configEntries = new ArrayList<>();

    public void addProperty(String key) {
        if(!entryExist(key)) {
            setProperty(key,"0");
            configEntries.add(key);
        }
    }

    public String getProperty(String key) {
        return config.getProperty(key, "0");
    }

    public void setProperty(String key, String value) {
        if(entryExist(key))
            config.setProperty(key, value);
    }

    public void writeProperties() {
        try {
            config.store(new FileOutputStream("bot.config"), null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void readProperties() {
        try {
            InputStream in = new FileInputStream("bot.config");
            config.load(in);
        } catch (FileNotFoundException e) {
            writeProperties();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean entryExist(String key){
        return configEntries.contains(key);
    }
}
