package util;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

public class Statics {

    public static Settings config = new Settings();
    public static FileHandler fileHandler;

    public static void init() {
        try {

            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy-HH:mm:ss");
            Timestamp timestamp = new Timestamp(date.getTime());
            fileHandler = new FileHandler(  System.getProperty("user.dir") + "/logs/" + simpleDateFormat.format(timestamp) + ".log");
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);

        } catch (SecurityException | IOException e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }

}
